package UDP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class TCP_CLIENTE_NRO_VOCALES {
	static Socket sCliente;
	static BufferedReader entrada;
	static PrintWriter salida;
	static Scanner mensajeb;
	public static void main(String[] args) {
		try {
			//ESTABLECEMOS CONEXION CON EL SERVIDOR PARA LO QUE HAY Q DEFINIR EL PUERTO Y EL HOST
			sCliente = new Socket("localhost",8888);
			//DEFINIMOS BUFFER DE ENTRADA Y PRINTWRITER PARA LOS MENSAJES DE SALIDA
			entrada= new BufferedReader(new InputStreamReader(sCliente.getInputStream()));
			salida=new PrintWriter(new BufferedWriter(new OutputStreamWriter(sCliente.getOutputStream())),true);
			//RECIBIMOS MENSAJE DEL SERVIDOR
			mensajeb = new Scanner(sCliente.getInputStream());
			//LO IMPRIMIMOS
			System.out.println(mensajeb.nextLine());
			
		} catch (Exception e) {
			e.printStackTrace();
			close();
		}
		
		BufferedReader sc=new BufferedReader(new InputStreamReader(System.in));
		try {
			//MIENTRAS EL CLIENTE NO ESCRIBA EXIT PODRA SEGUIR REALIZANDO PETICIONES AL SERVIDOR
			boolean sw=true;
			while(sw){
				//ESCRIBE MENSAJE ENVIADO POR EL SERVIDOR
				System.out.println(mensajeb.nextLine());
				//LEE UNA CADENA PARA ENVIARLA AL SERVIDOR
				String mensaje =sc.readLine();
				//ENVIA LA CADENA
				salida.println(mensaje);
				//SI LA CADENA ES EXIT DARA POR FINALIZADA LA CONEXIO
				if(mensaje.equals("exit")){
					break;
				}
				// SINO IMPRIMIRA LAS RESPUESTAS DEL SERVIDOR
				else{
					System.out.println(entrada.readLine());
				};
			}

		} catch (Exception e) {
			e.printStackTrace();
			close();
		}
		//CERRAMOS LA CONEXION CON EL SERVIDOR
		close();

		
	}
	public static void close(){
		try {
			mensajeb.close();
			salida.close();
			entrada.close();
			sCliente.close();
			System.out.println("----------conexion finalizada.........");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
