package UDP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class udpServer_primo {
	static private DatagramSocket UDP;
	
public static void main(String[] args) throws IOException {
	
		try {
			UDP = new DatagramSocket(8888);
			System.out.println(" --------- SERVIDOR INICIALIZADO ---------");
			System.out.println("------ En espera de solicitudes ------");
			
			while(true){
				DatagramPacket received = new DatagramPacket(new byte[1024],1024);
				UDP.receive(received);
				String nro= new String(received.getData());
				int num=Integer.parseInt(nro.trim());
				System.out.println("Numero: " +nro.trim());
				String p="es primo";
				if (!esprimo(nro.trim())) {
					p="NO es primo";
				}
				byte mensajeEnviar[] = new byte[1024];
				mensajeEnviar = p.getBytes();
				DatagramPacket paqueteAEnviar = new DatagramPacket(mensajeEnviar,
				mensajeEnviar.length,received.getAddress(),received.getPort());
				UDP.send(paqueteAEnviar);
				System.out.println("Cliente conectado: "+received.getPort()+" "+
				received.getAddress());
				
				
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}
	public static void close(){
		try {
			UDP.close();
			System.out.println("-----------LA CONEXION HA FINALIZADO------------");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static boolean esprimo(String numero){
		int n=Integer.parseInt(numero);
		System.out.println(n);
		for (int i = 2; i < n; i++) {
			if(n % i==0) return false;
		}
		return true;
	}
	public static String recortar(String num, int length){
		String pal="";
		for (int i = 0; i < length; i++) {
			pal+=num.charAt(i);
		}
		return pal;
	}
	
}
