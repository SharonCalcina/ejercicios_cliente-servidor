package UDP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class udpClient_primo {
	static private DatagramSocket UDP;
	
public static void main(String[] args) throws IOException {
	
		try {
			UDP = new DatagramSocket();
			System.out.println("----------INICIALIZANDO CONEXION--------------");
			byte mensajeE[] = new byte[1024];
			BufferedReader name= new BufferedReader(new InputStreamReader (System.in));
			while(true){
				String client=""+name.readLine();
				if(client.equals("0")) break;
				mensajeE = client.getBytes();
				InetAddress server = InetAddress.getByName("localhost");
				DatagramPacket saludo= new DatagramPacket(mensajeE,mensajeE.length,server,8888);
				UDP.send(saludo);
				
				byte mensajeR[] = new byte[1024];
				DatagramPacket received = new DatagramPacket(mensajeR,mensajeR.length);
				UDP.receive(received);
				String mensajeRecibido = new String(received.getData());
				System.out.println(mensajeRecibido);
			}
			
			close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void close(){
		try {
			UDP.close();
			System.out.println("-----------LA CONEXION HA FINALIZADO------------");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}